<?php get_header() ?>

<!-- Check to make sure there are posts. -->
<?php if (have_posts()) : ?>

	<!-- The WordPress Loop -->
	<?php while (have_posts()) : ?>
		
		<!-- Collects the information for the current post. -->
		<?php the_post() ?>

		<?php $customFields = get_post_custom(); ?>

		<article class="home_art">

			<a href="<?php the_permalink() ?>">
				<img 
					src="<?php echo $customFields['post_image'][0]; ?>" 
					class="title_card" 
					alt="Title Card"
				>
			</a>

			<?php if ($customFields['episode_number'][0]) : ?>
				<a href="<?php the_permalink() ?>">
					<span class="large_ep_num"><?php echo $customFields['episode_number'][0]; ?></span>
				</a>
			<?php endif; ?>

			<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>

			<p><?php echo $customFields['summary'][0]; ?></p>

			<?php if ($customFields['show_icon'][0]) : ?>
				<img src="<?php echo $customFields['show_icon'][0]; ?>" class="show_icon" alt="Show Icon" title="">
			<?php endif; ?>

			<span class="clearfix"></span>

		</article>
		
	<?php endwhile; ?>

	<!-- Post Navigation -->
	<div class="post_links">
		<span class="post_link_left">
			<?php next_posts_link('<< Older Posts') ?>
		</span>
		&nbsp;
		<span class="post_link_right">
			<?php previous_posts_link('Newer Posts >>') ?>
		</span>
	</div>

<!-- No posts found. -->
<?php else : ?>

	<h2>Nothing Found</h2>

<?php endif; ?>

<?php get_footer() ?>