<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo('charset') ?>">
		<title>
			<?php
				if (function_exists('is_tag') && is_tag())
				{
					single_tag_title('Tag: ');
					echo ' - ';
				}
				elseif (is_search())
				{
					echo 'Search for '.wp_specialchars($s).' - ';
				}
				elseif ( ! is_404() && (is_single() || is_page()))
				{
					wp_title('');
					echo ' - ';
				}
				elseif (is_404())
				{
					echo 'Not Found - ';
				}

				bloginfo('name');
			?>
		</title>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" type="text/css" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Cuprum:400,700' rel='stylesheet' type='text/css'>
		<?php wp_head() ?>
	</head>
	<body>
		<header role="banner">

			<div class="wrapper">

				<div class="logo">
					<a href="<?php echo home_url(); ?>" rel="nofollow">
						<img src="<?php bloginfo('template_directory') ?>/images/kl_logo.svg" />
					</a>
				</div>

				<h1>
					<a href="<?php echo home_url(); ?>" rel="nofollow">
						Knight<span>Lantern</span>
					</a>
				</h1>

				<!-- if you'd like to use the site description you can un-comment it below -->
				<?php // bloginfo('description'); ?>

				<nav role="navigation" class="clearfix">
					<?php wp_page_menu('show_home=1') ?>
				</nav>

				<div class="social">
					<a href="http://knightlantern.com/feed" class="social-icon" title="RSS">r</a>
					<a href="//twitter.com/knightlantern" class="social-icon" title="Twitter">t</a>
					<!-- <a href="//facebook.com/knightlantern" class="social-icon" title="Facebook">f</a>
					<a href="#" class="social-icon" title="iTunes">i</a> -->
				</div>

				<!-- <div class="search">
					<?php get_search_form( true ); ?>
				</div> -->

			</div> <!-- end #inner-header -->

		</header>
		<div class="wrapper">
			<section>