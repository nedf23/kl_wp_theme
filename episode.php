<?php
/*
Template Name Posts: Episode
*/
?>

<?php get_header() ?>

<!-- Check to make sure there is a post. -->
<?php if (have_posts()) : ?>
	
	<!-- Collects the information for the current post. -->
	<?php the_post() ?>

	<?php $customFields = get_post_custom(); ?>

	<article>

		<div class="post_title">
			<a href="<?php the_permalink() ?>">
				<?php echo $customFields['episode_number'][0]; ?>: <?php the_title() ?>
			</a>
		</div>

		<div class="metadata">
			<?php the_author() ?>, 
			<?php the_time('F j, Y') ?>
		</div>

		<div class="post_content">
			<div class="post_details">
		
				<p><?php echo $customFields['summary'][0]; ?></p>
		
				<?php echo do_shortcode('[audio mp3="'.$customFields['audio'][0].'" ogg=""]'); ?>
				<p><a href="<?php echo $customFields['audio'][0]; ?>">Download MP3</a></p>
		
				<?php if (isset($customFields['air_date'][0]) && $customFields['air_date'][0] != '') : ?>
					<p>
						<strong>Air Date:</strong>
						<?php echo $customFields['air_date'][0]; ?>
					</p>
				<?php endif; ?>
		
				<?php if (isset($customFields['cast'][0]) && $customFields['cast'][0] != '') : ?>
					<strong>Cast:</strong>
					<?php echo $customFields['cast'][0]; ?>
				<?php endif; ?>

				<img src="<?php echo $customFields['show_icon'][0]; ?>" class="show_icon" alt="Show Icon" title="">
			</div>

			<img 
				src="<?php echo $customFields['post_image'][0]; ?>" 
				class="title_card_large" 
				alt="Title Card"
			>

			<span class="clearfix"></span>
		</div>

		<!-- The post content. -->
		<?php the_content(); ?>

	</article>

	<article>

		<!-- The post comments with a form to add comments. -->
		<h2>Comments:</h2>
		<?php comments_template() ?>

	</article>

<!-- No post found. -->
<?php else : ?>

	<h2>Nothing Found</h2>

<?php endif; ?>

<?php get_footer() ?>