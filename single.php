<?php get_header() ?>

<!-- Check to make sure there is a post. -->
<?php if (have_posts()) : ?>
	
	<!-- Collects the information for the current post. -->
	<?php the_post() ?>

	<?php $customFields = get_post_custom(); ?>

	<article>

		<div class="post_title">
			<a href="<?php the_permalink() ?>">
				<?php the_title() ?>
			</a>
		</div>

		<div class="metadata">
			<?php the_author() ?>, 
			<?php the_time('F j, Y') ?>
		</div>

		<!-- The post content. -->
		<?php the_content(); ?>

	</article>

	<article>

		<!-- The post comments with a form to add comments. -->
		<h2>Comments:</h2>
		<?php comments_template() ?>

	</article>

<!-- No post found. -->
<?php else : ?>

	<h2>Nothing Found</h2>

<?php endif; ?>

<?php get_footer() ?>